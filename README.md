# WTTJ technical test

This repository implements a simple app witch shows a jobs list and a details modal for each jobs.

## How to launch the project

* First you need to install the dependencies: `npm install`
* You can now run `npm start`, it will open a browser tab with the server address

To change the api url and version, you can edit `src/config.js` and replace the values with your own.<br/>
To run the tests, see the section below.

## Available Scripts

In the project directory, you can run:

### Starting the project `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run test:e2e`

Launches the cypress test runner in text mode.<br />
See the cypress documentation for more informations [running tests](https://docs.cypress.io/guides/guides/command-line.html#Commands) for more information.

Before running e2e tests please make sure that the react project reponds to the `baseUrl` specified into the cypress.json file.

### `npm run test:e2e-ui`

Same as `npm test:e2e` but it starts cypress in UI mode.

### `npm run lint`

Launches the eslint test.<br/>
Eslint config is based on [airbnb test config](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb)

### `npm run build`

Builds the app for production to the `build` folder.
