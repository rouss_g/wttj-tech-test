// react core
import React from 'react';
import PropTypes from 'prop-types';

// components
import { Form } from 'react-final-form';
import {
    Box,
    ConnectedField,
    InputText,
    Select,
    DatePicker,
} from 'welcome-ui';

// lib
import searchJobs from '../lib/searchJobs';


function JobSearchForm(props) {
    const { onResultsChange, jobs } = props;

    return (
        <Form
            onSubmit={({ searchFullText, groupBy, ...searchOptions }) => {
                onResultsChange(searchJobs(
                    jobs,
                    searchFullText,
                    {
                        'contract_type.en': { search: searchOptions.contract_type, op: 'eq', type: 'string' },
                        published_at: { search: searchOptions.published_at, op: 'gt', type: 'date' },
                    },
                    groupBy,
                ), searchFullText);
            }}
            initialValues={{
                groupBy: 'department.name',
            }}
            render={({ handleSubmit, form }) => (
                <form onChange={handleSubmit} data-cy="job-search-container">
                    <Box display="flex" justifyContent="space-between" flexWrap="wrap">
                        <Box>
                            <ConnectedField
                                name="searchFullText"
                                component={InputText}
                                placeholder="Your dream job?"
                                label="Your dream job?"
                                data-cy="job-search-fulltext-input"
                            />
                        </Box>
                        <Box>
                            <ConnectedField
                                name="contract_type"
                                component={Select}
                                options={[
                                    { value: 'Full-Time', label: 'Full-Time' },
                                    { value: 'Internship', label: 'Internship' },
                                    { value: 'Temporary', label: 'Temporary' },
                                    { value: 'Other', label: 'Other' },
                                    { value: 'Part-Time', label: 'Part-Time' },
                                    { value: 'Apprenticeship', label: 'Apprenticeship' },
                                ]}
                                isClearable
                                label="Contract type"
                                onChange={(val) => {
                                    form.change('contract_type', val);
                                    form.submit();
                                }}
                            />
                        </Box>
                        <Box>
                            <ConnectedField
                                component={DatePicker}
                                name="published_at"
                                placeholder="Published after"
                                label="Published after"
                                onChange={(val) => {
                                    form.change('published_at', val);
                                    form.submit();
                                }}
                            />
                        </Box>
                        <Box>
                            <ConnectedField
                                name="groupBy"
                                component={Select}
                                isClearable
                                options={[
                                    { value: 'office.name', label: 'City' },
                                    { value: 'department.name', label: 'Department' },
                                ]}
                                label="Group by"
                                onChange={(val) => {
                                    form.change('groupBy', val);
                                    form.submit();
                                }}
                            />
                        </Box>
                    </Box>
                </form>
            )}
        />
    );
}

JobSearchForm.propTypes = {
    jobs: PropTypes.array.isRequired,
    onResultsChange: PropTypes.func.isRequired,
};

export default JobSearchForm;
