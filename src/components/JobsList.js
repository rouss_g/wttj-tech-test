// react core
import React from 'react';
import PropTypes from 'prop-types';

// components
import JobCard from './JobCard';

function JobsList(props) {
    const { jobs, onSelectedJob, highlightTexts } = props;

    return (jobs.map((job) => (
        <JobCard
            data-cy="job-card"
            key={`job-card-${job.id}`}
            job={job}
            highlightTexts={highlightTexts}
            onClick={() => onSelectedJob(job)}
        />
    )));
}

JobsList.propTypes = {
    jobs: PropTypes.array.isRequired,
    onSelectedJob: PropTypes.func.isRequired,
    highlightTexts: PropTypes.array,
};

JobsList.defaultProps = {
    highlightTexts: [],
};

export default JobsList;
