// react core
import React from 'react';
import PropTypes from 'prop-types';

// components
import {
    Box,
    Text,
    Icon,
    Button,
} from 'welcome-ui';
import Card from './styled/Card';
import HighlightText from './HighlightText';

function JobCard(props) {
    const { job, onClick, highlightTexts } = props;

    return (
        <Card justifyContent="space-between" flexWrap="wrap" data-cy="job-card">
            <Box>
                <HighlightText
                    highlightTexts={highlightTexts}
                    text={job.name}
                    variant="h4"
                    color="rgb(55, 57, 66)"
                    marginTop={0}
                    marginBottom={5}
                    fontWeight="bold"
                    data-cy="job-title"
                    keyPrefix={`job-title-card-${job.id}`}
                />
                <Box display="flex" alignItems="center" color="rgb(151, 153, 157)">
                    <Icon name="handshake" />
                    <Text data-cy="job-contract" fontSize={12} marginRight={5} marginLeft={5} marginTop={0} marginBottom={0}>{job.contract_type.en}</Text>
                    <Icon name="map" />
                    <Text data-cy="job-location" fontSize={12} marginRight={5} marginLeft={5} marginTop={0} marginBottom={0}>{job.office.name}</Text>
                    <Icon name="date" />
                    <Text fontSize={12} marginRight={5} marginLeft={5} marginTop={0} marginBottom={0}>{new Date(job.published_at).toLocaleDateString()}</Text>
                    <Icon name="department" />
                    <Text data-cy="job-department" fontSize={12} marginRight={5} marginLeft={5} marginTop={0} marginBottom={0}>{job.department.name}</Text>
                </Box>
            </Box>
            <Box display="flex" alignItems="center">
                <Button onClick={onClick} data-cy="see-more">See more</Button>
            </Box>
        </Card>
    );
}

JobCard.propTypes = {
    job: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
    highlightTexts: PropTypes.array,
};

JobCard.defaultProps = {
    highlightTexts: [],
};


export default JobCard;
