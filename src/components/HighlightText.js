/* eslint-disable react/jsx-props-no-spreading */
// react core
import React from 'react';
import PropTypes from 'prop-types';

// components
import { Text } from 'welcome-ui';
import HighlightSpan from './styled/HighlightSpan';

// utils
import '../lib/regexEscape';

function HighlightText(props) {
    const {
        highlightTexts,
        text,
        keyPrefix,
        ...otherProps
    } = props;
    let textParts = [text];
    let keyIndex = 0;

    if (highlightTexts.length) {
        highlightTexts.forEach((hText) => {
            const newTextParts = [];
            textParts.forEach((textPart) => {
                if (typeof (textPart) !== 'string') {
                    newTextParts.push(textPart);
                    return;
                }
                const parts = textPart.split(new RegExp(`(${RegExp.escape(hText)})`, 'gi'));
                parts.forEach((part) => {
                    if (part.toLowerCase() === hText.toLowerCase()) {
                        newTextParts.push(
                            <HighlightSpan
                                key={`${keyPrefix}-${keyIndex++}`}
                                highlightColor="#a3a0ee"
                            >
                                {part}
                            </HighlightSpan>,
                        );
                    } else {
                        newTextParts.push(part);
                    }
                });
            });
            textParts = newTextParts;
        });
    }

    return (
        <Text {...otherProps}>
            {textParts}
        </Text>
    );
}

HighlightText.propTypes = {
    text: PropTypes.string.isRequired,
    keyPrefix: PropTypes.string.isRequired,
    highlightTexts: PropTypes.array,
};

HighlightText.defaultProps = {
    highlightTexts: [],
};

export default HighlightText;
