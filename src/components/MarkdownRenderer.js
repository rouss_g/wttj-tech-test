/* eslint-disable react/jsx-props-no-spreading */
// react core
import React from 'react';
import PropTypes from 'prop-types';

// components
import { Text, Link } from 'welcome-ui';
import ReactMardown from 'react-markdown';
import HtmlParser from 'react-markdown/plugins/html-parser';
import HtmlToReact from 'html-to-react';

const processNodeDefinitions = new HtmlToReact.ProcessNodeDefinitions(React);
const parseHtml = new HtmlParser({
    processingInstructions: [
        {
            shouldProcessNode: (node) => node.type === 'text' || ['a', 'strong'].indexOf(node.name) > -1,
            processNode: (node) => {
                if (node.type === 'text') {
                    return <ReactMardown disallowedTypes={['paragraph']} unwrapDisallowed source={node.data} />;
                }
                switch (node.name) {
                    case 'a':
                        if (node.children.length > 0 && node.children[0].type === 'text') {
                            return <Link {...node.attribs}>{node.children[0].data}</Link>;
                        }
                        return <Link {...node.attribs} />;
                    case 'strong':
                        return <Text variant="h4" as="strong" fontWeight="medium" />;
                    default:
                        return null;
                }
            },
        },
        {
            shouldProcessNode: () => true,
            processNode: processNodeDefinitions.processDefaultNode,
        },
    ],
});

function MarkdownRenderer(props) {
    const { source } = props;

    return (
        <ReactMardown
            source={source.replace(/<(\/?|!?)(em|p|script)>/g, '')}
            escapeHtml={false}
            astPlugins={[parseHtml]}
            renderers={{
                strong: (mdProps) => <Text variant="h4" as="strong" fontWeight="medium" {...mdProps} />,
                emphasis: (mdProps) => <Text variant="h4" as="strong" fontWeight="medium" {...mdProps} />,
                link: (mdProps) => <Link {...mdProps} />,
            }}
        />
    );
}

MarkdownRenderer.propTypes = {
    source: PropTypes.string.isRequired,
};

export default MarkdownRenderer;
