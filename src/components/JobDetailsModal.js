// react core
import React from 'react';
import PropTypes from 'prop-types';

// components
import { Text, Button, Box } from 'welcome-ui';
import Modal, { ModalHeader, ModalBody } from './styled/Modal';
import MarkdownRenderer from './MarkdownRenderer';

function JobDetailsModal(props) {
    const { job, onDismiss } = props;

    const jobApplyWebsite = job ? job.websites_urls.find((website) => website.website_reference === 'wttj_fr') : null;
    const jobApplyUrl = jobApplyWebsite ? jobApplyWebsite.url : '#';
    return (
        <Modal
            isOpen={!!job}
            onDismiss={onDismiss}
            data-cy="job-details-modal"
        >
            {job ? (
                <>
                    <ModalHeader title={job.name} onDismiss={onDismiss} />
                    <ModalBody>
                        <Text variant="h3" fontWeight="bold">Job description</Text>
                        <MarkdownRenderer source={job.description} />
                        <hr size="1" />
                        <Text variant="h3" fontWeight="bold">Researched profile</Text>
                        <MarkdownRenderer source={job.profile} />
                        <hr size="1" />
                        <Text variant="h3" fontWeight="bold">Recruitment process</Text>
                        <MarkdownRenderer source={job.recruitment_process} />
                        <Box textAlign="center">
                            <Button as="a" width="10rem" href={jobApplyUrl}>Apply</Button>
                        </Box>
                    </ModalBody>
                </>
            ) : null}
        </Modal>
    );
}

JobDetailsModal.propTypes = {
    onDismiss: PropTypes.func.isRequired,
    job: PropTypes.object,
};

JobDetailsModal.defaultProps = {
    job: null,
};

export default JobDetailsModal;
