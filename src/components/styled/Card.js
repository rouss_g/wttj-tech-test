import styled from 'styled-components';

const Card = styled.div`
    display: flex;
    margin-bottom: 1.25rem;
    margin-top: 1.25rem;
    padding: 1.25rem;
    border-radius: 5px;
    border: 1px solid rgb(238,238,238);
    background-color: rgb(255,255,255);
    transition: all 0.2s cubic-bezier(0.41,0.094,0.54,0.07) 0s;
    box-shadow: rgba(0, 0, 0, 0.05) 1px 2px 4px;

    &:hover {
        transform: translateY(-10px);
        box-shadow: rgba(0, 0, 0, 0.07) 1px 7px 15px;
    }
    ${(props) => props}
`;

export default Card;
