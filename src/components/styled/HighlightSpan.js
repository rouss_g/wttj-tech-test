import styled from 'styled-components';

const HighlightSpan = styled.span`
    background-color: ${(props) => props.highlightColor}
`;

export default HighlightSpan;
