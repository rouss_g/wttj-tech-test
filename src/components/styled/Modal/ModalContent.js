import styled from 'styled-components';

const ModalContent = styled.div`
    position: relative;
    width: auto;
    margin: .5rem;
    max-width: 57rem;
    margin: 1.75rem auto;
    background-color: white;
    pointer-event: none;
`;

export default ModalContent;
