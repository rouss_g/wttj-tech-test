// react core
import React from 'react';
import PropTypes from 'prop-types';

// components
import {
    Box,
    Text,
    Icon,
    Button,
} from 'welcome-ui';

function ModalHeader(props) {
    const { onDismiss, title } = props;

    return (
        <Box
            padding="1rem"
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            borderBottom="1px solid #e9ecef"
        >
            <Text variant="h4" margin="0">{title}</Text>
            <Button variant="secondary" border="none" onClick={onDismiss}>
                <Icon name="cross" />
            </Button>
        </Box>
    );
}

ModalHeader.propTypes = {
    title: PropTypes.string.isRequired,
    onDismiss: PropTypes.func.isRequired,
};

export default ModalHeader;
