/* eslint-disable react/jsx-props-no-spreading */
// react core
import React from 'react';
import ReactDOM from 'react-dom';

// components
import ModalContent from './ModalContent';
import ModalOverlay from './ModalOverlay';
import _ModalHeader from './ModalHeader';
import _ModalBody from './ModalBody';

export default function Modal(props) {
    const {
        children,
        isOpen,
        onDismiss,
        ...otherProps
    } = props;
    const body = document.querySelector('body');
    let modal = null;

    if (isOpen) {
        body.style = 'overflow: hidden;';
        modal = (
            <ModalOverlay onClick={onDismiss}>
                <ModalContent {...otherProps} onClick={(e) => e.stopPropagation()}>
                    {children}
                </ModalContent>
            </ModalOverlay>
        );
    } else {
        body.style = 'overflow: unset;';
    }
    return ReactDOM.createPortal(modal, body);
}

export const ModalHeader = _ModalHeader;
export const ModalBody = _ModalBody;
