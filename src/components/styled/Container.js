import styled from 'styled-components';

const Container = styled.div`
    width: 100%;
    max-width: 56rem;
    margin-left: auto;
    margin-right: auto;
    ${(props) => props}
`;

export default Container;
