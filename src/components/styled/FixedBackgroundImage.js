import styled from 'styled-components';

const FixedBackgroundImage = styled.div`
    height: 30rem;
    background-image: url("https://cdn.welcometothejungle.co/uploads/website_organization/cover_image/wttj_fr/fr-wttj.jpg");
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    filter: brightness(40%);
    position: fixed;
    width: 100%;
    z-index: -1;
    ${(props) => props}
`;

export default FixedBackgroundImage;
