// react
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// theme
import { ThemeProvider } from '@xstyled/styled-components';
import { createTheme, GlobalStyle } from 'welcome-ui';

// containers
import JobsListContainer from './containers/JobsListContainer';

const theme = createTheme({});

export default function Root() {
    return (
        <ThemeProvider theme={theme}>
            <>
                <GlobalStyle />
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={JobsListContainer} />
                    </Switch>
                </BrowserRouter>
            </>
        </ThemeProvider>
    );
}
