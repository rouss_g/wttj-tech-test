
/**
 * @function resolve
 * Resolve a given path
 * @param {String|Array} path path to an object field
 * @param {Object} obj object where the field is located
 * @param {String} separator optional - default to '.' - this is th separator used by split when the path is a string
 * @returns {*} the corresponding field value
 */
export default function resolve(path, obj, separator = '.') {
    const properties = Array.isArray(path) ? path : path.split(separator);
    return properties.reduce((prev, curr) => prev && prev[curr], obj);
}
