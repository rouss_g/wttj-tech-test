/* eslint-disable no-console */

function log(message, ...optionalParams) {
    console.log(message, ...optionalParams);
}

function error(text, ...optionalParams) {
    console.error(text, ...optionalParams);
}

export default {
    log,
    error,
};
