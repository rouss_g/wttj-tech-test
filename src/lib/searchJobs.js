import resolve from './resolve';

/**
 * @function compare
 * Compare two values
 * @param {*} value1 left value to compare
 * @param {*} value2 right value to compare
 * @param {String} op eq|gt
 * @returns {Boolean}
 */
function compare(value1, value2, op) {
    switch (op) {
        case 'eq':
            return value1 === value2;
        case 'gt':
            return value1 > value2;
        default:
            return false;
    }
}

/**
 * @function searchJobs
 * Sort and filter a list of jobs
 * @param {Array} jobsList formatted list of jobs from apiWrapper.company.get().jobs
 * @param {String} searchFullText the text to search for into name, description and profile field
 * @param {Object} fieldSearchOptions object containing a field name as key and as value an object like this: {op: 'gt|eq', search: 'my search', type: 'date|string'}
 * @param {String} groupByField string containing a field to group by (office.name or department.name)
 * @returns {Array} a new job list
 */
export default function searchJobs(jobsList, searchFullText, fieldSearchOptions, groupByField) {
    const filteredJobs = jobsList.filter((job) => {
        // search full text
        if (searchFullText) {
            const textParts = searchFullText.split(' ');

            for (let i = 0; i < textParts.length; i++) {
                if (!job.name.toLowerCase().includes(textParts[i].toLowerCase())
                    && !job.description.toLowerCase().includes(textParts[i].toLowerCase())
                    && !job.profile.toLowerCase().includes(textParts[i].toLowerCase())) {
                    return false;
                }
            }
        }

        // search by field
        const keys = Object.keys(fieldSearchOptions);
        for (let i = 0; i < keys.length; i++) {
            const searchConfig = fieldSearchOptions[keys[i]];

            if (searchConfig && searchConfig.search) {
                const fieldValue = resolve(keys[i], job, '.');

                switch (searchConfig.type) {
                    case 'date':
                        if (!compare(new Date(fieldValue).getTime(), new Date(searchConfig.search).getTime(), searchConfig.op)) {
                            return false;
                        }
                        break;
                    case 'string':
                        if (!compare(fieldValue, searchConfig.search, searchConfig.op)) {
                            return false;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        return true;
    });

    if (groupByField) {
        filteredJobs.sort((a, b) => {
            const fieldA = resolve(groupByField, a, '.');
            const fieldB = resolve(groupByField, b, '.');
            if (fieldA < fieldB) {
                return -1;
            }
            if (fieldA > fieldB) {
                return 1;
            }
            return 0;
        });
    }
    return filteredJobs;
}
