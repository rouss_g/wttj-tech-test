RegExp.escape = (str) => str.replace(/[-[\]{}()*+!<=:?./\\^$|#\s,]/g, '\\$&');
