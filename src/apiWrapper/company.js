import axios from 'axios';
import config from '../config';

async function get(companyId) {
    const response = await axios.get(`${config.apiUrl}/${config.apiVersion}/embed?organization_reference=${companyId}`);
    return response.data;
}

export default {
    get,
};
