// react core
import React, { useState, useEffect } from 'react';

// components
import {
    Text,
    Alert,
    AlertTitle,
    Box,
} from 'welcome-ui';
import Container from '../components/styled/Container';
import FixedBackgroundImage from '../components/styled/FixedBackgroundImage';
import JobsList from '../components/JobsList';
import JobDetailsModal from '../components/JobDetailsModal';
import JobSearchForm from '../components/JobSearchForm';

// api
import apiWrapper from '../apiWrapper';

// lib
import searchJobs from '../lib/searchJobs';
import Logger from '../lib/Logger';

export default function JobsListContainer() {
    const [company, setCompany] = useState({ jobs: [], name: '' });
    const [jobSearchResults, setJobSearchResults] = useState([]);
    const [highlightTexts, setHighlightTexts] = useState([]);
    const [selectedJob, setSelectedJob] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState('');

    useEffect(() => {
        (async () => {
            try {
                const fetchedCompany = await apiWrapper.company.get('Pg4eV6k');
                setCompany(fetchedCompany);
                setJobSearchResults(searchJobs(fetchedCompany.jobs, null, {}, 'department.name'));
            } catch (e) {
                Logger.error('Error occured while trying to fetch data from apiWrapper.company.get', e);
                setError('Something went wrong on our end');
            }
            setIsLoading(false);
        })();
    }, []);

    return (
        <Box data-cy="main-container">
            <JobDetailsModal job={selectedJob} onDismiss={() => setSelectedJob(null)} />
            <FixedBackgroundImage />
            <Box marginBottom={20}>
                <Text
                    margin={0}
                    paddingTop="10rem"
                    paddingBottom="5rem"
                    color="white"
                    variant="h1"
                    textAlign="center"
                    fontWeight="medium"
                >
                    {company.name}
                </Text>
                <Container backgroundColor="#FAFAFA" padding={20} border="1px solid rgb(238,238,238)">
                    <Text variant="h3" fontWeight="medium" textAlign="center">
                        Our offers
                    </Text>
                    <JobSearchForm
                        jobs={company.jobs}
                        onResultsChange={(jobs, searchFullText) => {
                            setJobSearchResults(jobs);
                            setHighlightTexts(searchFullText && searchFullText.split(' '));
                        }}
                    />
                    {isLoading ? (
                        <Text textAlign="center">Loading, please wait ...</Text>
                    ) : null }
                    {error ? (
                        <Alert data-cy="error">
                            <AlertTitle>{error}</AlertTitle>
                        </Alert>
                    ) : null}
                    {!isLoading && !error && !jobSearchResults.length ? (
                        <Text textAlign="center">
                            Sorry, we can&apos;t find any jobs corresponding to your research.
                        </Text>
                    ) : null}
                    <JobsList
                        onSelectedJob={(job) => setSelectedJob(job)}
                        jobs={jobSearchResults}
                        highlightTexts={highlightTexts}
                    />
                </Container>
            </Box>
        </Box>
    );
}
