describe('Job research', () => {
    beforeEach(() => {
        cy.server();
        cy.route('**/embed**', 'fixture:companyWithManyJobs.json').as('getCompanyJobs');
        cy.visit('/');
    })

    it('Renders', () => {
        cy.get('[data-cy=job-search-container]').should('be.visible');
    })

    it('Can search by fulltext on job.name', () => {
        cy.get('input[data-cy=job-search-fulltext-input]').type('Technical');
        cy.get('[data-cy=job-card]')
            .should('have.length', 1)
            .should("be.visible");
    })
})