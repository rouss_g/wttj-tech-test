describe('Job list', () => {
    beforeEach(() => {
        cy.server();
    })

    it('Renders', () => {
        cy.route('**/embed**', 'fixture:companyWithManyJobs.json').as('getCompanyJobs');
        cy.visit('/');
        cy.get('[data-cy=main-container]').should('be.visible');
    })

    it('Can load and display jobs', () => {
        cy.route('**/embed**', 'fixture:companyWithManyJobs.json').as('getCompanyJobs');
        cy.visit('/');
        cy.get('[data-cy=job-card]')
            .should('have.length', 3)
            .should("be.visible");
    })

    it('Shows an alert when a network error occurs', () => {
        cy.route({
            url: '**/embed**',
            response: 'fixture:companyWithManyJobs.json',
            status: 404,
        }).as('getCompanyJobs');
        cy.visit('/');
        cy.get('[data-cy=error]').should('be.visible');
    })

    it('Show all the correct informations into a job card', () => {
        cy.fixture('companyWithASingleJob.json').then((company) => {
            cy.route('**/embed**', company).as('getCompanyJob');
            cy.visit('/');
            cy.get('[data-cy=job-title]').contains(company.jobs[0].name);
            cy.get('[data-cy=job-contract]').contains(company.jobs[0].contract_type.en);
            cy.get('[data-cy=job-location]').contains(company.jobs[0].office.name);
            cy.get('[data-cy=job-department]').contains(company.jobs[0].department.name);
        });
    })

    it('Can show a modal containing the job details', () => {
        cy.fixture('companyWithASingleJob.json').then((company) => {
            cy.route('**/embed**', company).as('getCompanyJob');
            cy.visit('/');
            cy.get('[data-cy=see-more]').click();
            cy.get('[data-cy=job-details-modal]').should('exist');
        });
    })
})